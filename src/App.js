import logo from './logo.svg';
import './App.css';
import ShoeShop from './ShoeShop/ShoeShop';
import { dataShoe } from './ShoeShop/data';

function App() {

  return (
    <div className="App">

      <ShoeShop />
    </div>
  );
}

export default App;
