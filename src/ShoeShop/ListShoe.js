import React, { useEffect, useState } from 'react'
import ItemShoe from './ItemShoe'
import { dataShoe } from './data'
export default function ListShoe() {

    const [shoeArr, setShoeArr] = useState([])
    useEffect(() => {
        console.log("list shoe", dataShoe);
        setShoeArr(dataShoe);
    }, [])
    let listShoe = (item) => {
        return shoeArr.map((item) => {
            return (
                <ItemShoe shoe={item} />
            )
        })
    }
    return (
        <div className='row' style={{ justifyContent: 'space-evenly' }}>
            {listShoe()}
        </div>
    )
}
