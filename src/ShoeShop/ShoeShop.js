import React from 'react'
import Cart from './Cart';

import ListShoe from './ListShoe';
export default function ShoeShop() {

    return (
        <div className='container text-center'>
            <h1>Shoe Shop</h1>
            <Cart />
            <ListShoe />
        </div>
    )
}
