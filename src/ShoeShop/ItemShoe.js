import { Button, Card } from 'antd'
import Meta from 'antd/es/card/Meta'
import React from 'react'

export default function ItemShoe(props) {
    let { shoe } = props
    console.log("🚀 ~ file: ItemShoe.js:7 ~ ItemShoe ~ props:", props)
    return (
        <Card
            className='col-3 mx-3 my-2'
            hoverable
            style={{ width: 240, }}
            cover={<img alt="example" src={shoe.image} />}
        >
            <Meta title={shoe.name} description={shoe.shortDescription} />
            <Button style={{ margin: '10px' }} type={'primary'} danger>Add to cart</Button>
        </Card>
    )
}
